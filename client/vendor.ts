// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

// RxJS
import 'rxjs';

// Other dependencies
import 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'primeng/primeng'
import 'angular-2-local-storage';
import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css';
import '!style-loader!css-loader!font-awesome/css/font-awesome.min.css';
import '!style-loader!css-loader!primeng/resources/primeng.min.css';
import '!style-loader!css-loader!primeng/resources/themes/bootstrap/theme.css';