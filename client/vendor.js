"use strict";
// Angular
require('@angular/platform-browser');
require('@angular/platform-browser-dynamic');
require('@angular/core');
require('@angular/common');
require('@angular/http');
require('@angular/router');
// RxJS
require('rxjs');
// Other dependencies
require('jquery');
require('bootstrap/dist/js/bootstrap');
require('primeng/primeng');
require('angular-2-local-storage');
require('!style-loader!css-loader!bootstrap/dist/css/bootstrap.css');
require('!style-loader!css-loader!font-awesome/css/font-awesome.min.css');
require('!style-loader!css-loader!primeng/resources/primeng.min.css');
require('!style-loader!css-loader!primeng/resources/themes/bootstrap/theme.css');
//# sourceMappingURL=vendor.js.map