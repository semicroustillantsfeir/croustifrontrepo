import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './components/app.component';
import { routing, appRoutingProviders } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { StepsModule } from 'primeng/components/steps/steps';
import { JourneyPlannerComponent } from './components/journey-planner/journey-planner.component';
import { JourneyPlannerWhereComponent } from './components/journey-planner-where/journey-planner-where.component';
import { GoogleplaceDirective } from './shared/directives/google-map-autocomplete.directive';
import { GrowlModule } from 'primeng/components/growl/growl';
import { WhenComponent } from './components/journey-planner-when/journey-planner-when.component';
import { SelectButtonModule } from 'primeng/components/selectbutton/selectbutton';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { ButtonModule } from 'primeng/components/button/button';
import { GeolocationService } from "./shared/services/geolocation.service";
import {JourneyPlannerParkComponent} from "./components/journey-planner-park/journey-planner-park.component";
import {JourneyPlannerService} from "./shared/services/journey-planner.service";
import { DialogModule } from "primeng/components/dialog/dialog";
import {DataGridModule} from "primeng/components/datagrid/datagrid";
import {PanelModule} from "primeng/components/panel/panel";
import {JourneyPlannerRecapitulationComponent} from "./components/journey-planner-recapitulation/journey-planner-recapitulation.component";
import {AppService} from "./shared/services/app.service";
import {TripComponent} from "./components/trip/trip.component";
import { LocalStorageModule } from 'angular-2-local-storage';
import {AccordionModule} from "primeng/primeng";


@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		routing,
		appRoutingProviders,
		StepsModule,
		SelectButtonModule,
		CalendarModule,
		GrowlModule,
		ButtonModule,
		DialogModule,
		ButtonModule,
		DataGridModule,
		AccordionModule,
		PanelModule,
		LocalStorageModule.withConfig({
			prefix:'my-app',
			storageType: 'localStorage'
		})
	],
	declarations: [
		AppComponent,
		JourneyPlannerComponent,
		JourneyPlannerWhereComponent,
		GoogleplaceDirective,
		WhenComponent,
		JourneyPlannerParkComponent,
		JourneyPlannerRecapitulationComponent,
		TripComponent,

	],
	providers: [
		JourneyPlannerService,
		GeolocationService,
		AppService,
	],
	bootstrap: [AppComponent]
})

export class AppModule { }
