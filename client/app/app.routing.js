"use strict";
var router_1 = require('@angular/router');
var trip_component_1 = require("./components/trip/trip.component");
var appRoutes = [
    {
        path: 'trips',
        component: trip_component_1.TripComponent
    }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map