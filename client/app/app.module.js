"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var app_component_1 = require('./components/app.component');
var app_routing_1 = require('./app.routing');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var steps_1 = require('primeng/components/steps/steps');
var journey_planner_component_1 = require('./components/journey-planner/journey-planner.component');
var journey_planner_where_component_1 = require('./components/journey-planner-where/journey-planner-where.component');
var google_map_autocomplete_directive_1 = require('./shared/directives/google-map-autocomplete.directive');
var growl_1 = require('primeng/components/growl/growl');
var journey_planner_when_component_1 = require('./components/journey-planner-when/journey-planner-when.component');
var selectbutton_1 = require('primeng/components/selectbutton/selectbutton');
var calendar_1 = require('primeng/components/calendar/calendar');
var button_1 = require('primeng/components/button/button');
var geolocation_service_1 = require("./shared/services/geolocation.service");
var journey_planner_park_component_1 = require("./components/journey-planner-park/journey-planner-park.component");
var journey_planner_service_1 = require("./shared/services/journey-planner.service");
var dialog_1 = require("primeng/components/dialog/dialog");
var datagrid_1 = require("primeng/components/datagrid/datagrid");
var panel_1 = require("primeng/components/panel/panel");
var journey_planner_recapitulation_component_1 = require("./components/journey-planner-recapitulation/journey-planner-recapitulation.component");
var app_service_1 = require("./shared/services/app.service");
var trip_component_1 = require("./components/trip/trip.component");
var angular_2_local_storage_1 = require('angular-2-local-storage');
var primeng_1 = require("primeng/primeng");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                app_routing_1.routing,
                app_routing_1.appRoutingProviders,
                steps_1.StepsModule,
                selectbutton_1.SelectButtonModule,
                calendar_1.CalendarModule,
                growl_1.GrowlModule,
                button_1.ButtonModule,
                dialog_1.DialogModule,
                button_1.ButtonModule,
                datagrid_1.DataGridModule,
                primeng_1.AccordionModule,
                panel_1.PanelModule,
                angular_2_local_storage_1.LocalStorageModule.withConfig({
                    prefix: 'my-app',
                    storageType: 'localStorage'
                })
            ],
            declarations: [
                app_component_1.AppComponent,
                journey_planner_component_1.JourneyPlannerComponent,
                journey_planner_where_component_1.JourneyPlannerWhereComponent,
                google_map_autocomplete_directive_1.GoogleplaceDirective,
                journey_planner_when_component_1.WhenComponent,
                journey_planner_park_component_1.JourneyPlannerParkComponent,
                journey_planner_recapitulation_component_1.JourneyPlannerRecapitulationComponent,
                trip_component_1.TripComponent,
            ],
            providers: [
                journey_planner_service_1.JourneyPlannerService,
                geolocation_service_1.GeolocationService,
                app_service_1.AppService,
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map