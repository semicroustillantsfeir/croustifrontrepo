import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'journey-planner-recapitulation',
    template:require('./journey-planner-recapitulation.component.html'),
    styles:[require('./journey-planner-recapitulation.component.scss').toString()]
})
export class JourneyPlannerRecapitulationComponent implements OnInit {
    @Input() journeyData: any;
    @Output() onNext = new EventEmitter<any>();

    constructor() { }

    ngOnInit() { }

    performNext() {
        this.onNext.emit(true);
    }

}