"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var WhenComponent = (function () {
    function WhenComponent() {
        this.onNext = new core_1.EventEmitter();
        this.types = [
            { label: 'Departure', value: 'Departure' },
            { label: 'Arrival', value: 'Arrival' }
        ];
        this.errorMessage = [];
    }
    WhenComponent.prototype.ngOnInit = function () {
        this.selectedType = this.types[0].value;
        this.minDate = new Date;
    };
    /**
     * Verify if the user select a date
     */
    WhenComponent.prototype.verifyIsAllCompleted = function () {
        if (!this.arrivalDepartureDate) {
            this.errorMessage.push({ severity: 'error', summary: 'Error Message', detail: 'Date is not selected' });
        }
        else {
            this.onNext.emit({ date: this.arrivalDepartureDate, type: this.selectedType });
        }
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], WhenComponent.prototype, "onNext", void 0);
    WhenComponent = __decorate([
        core_1.Component({
            selector: 'journey-planner-when',
            template: require('./journey-planner-when.component.html'),
            styles: [require('./journey-planner-when.component.scss').toString()]
        }), 
        __metadata('design:paramtypes', [])
    ], WhenComponent);
    return WhenComponent;
}());
exports.WhenComponent = WhenComponent;
//# sourceMappingURL=journey-planner-when.component.js.map