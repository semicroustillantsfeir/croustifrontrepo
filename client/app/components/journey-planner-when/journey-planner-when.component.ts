import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { SelectItem, Message } from "primeng/primeng";

@Component({
	selector: 'journey-planner-when',
	template: require('./journey-planner-when.component.html'),
	styles: [require('./journey-planner-when.component.scss').toString()]
})
export class WhenComponent implements OnInit {

	@Output() onNext = new EventEmitter<{date: Date, type: string}>();
	private types: SelectItem[] = [
		{label: 'Departure', value: 'Departure'},
		{label: 'Arrival', value: 'Arrival'}
	];
	private selectedType: string;
	private arrivalDepartureDate: Date;
	private errorMessage: Message[] = [];
	private minDate: Date;


	constructor() {
	}

	ngOnInit() {
		this.selectedType = this.types[0].value;
		this.minDate = new Date

	}

	/**
	 * Verify if the user select a date
	 */

	verifyIsAllCompleted() {
		if (!this.arrivalDepartureDate) {
			this.errorMessage.push({severity: 'error', summary: 'Error Message', detail: 'Date is not selected'})
		} else {
			this.onNext.emit({date: this.arrivalDepartureDate, type: this.selectedType});
		}
	}

}