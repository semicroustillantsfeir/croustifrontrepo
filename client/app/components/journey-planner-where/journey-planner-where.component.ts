import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Message } from "primeng/components/common/api";

@Component({
	selector: 'journey-planner-where',
	template: require('./journey-planner-where.component.html'),
	styles: [require('./journey-planner-where.component.scss').toString()]
})
export class JourneyPlannerWhereComponent implements OnInit {
	private addressObject: any;
	private messages: Array<Message>;

	@Output() onNext = new EventEmitter<any>();

	constructor() {
	}

	ngOnInit() {
		this.messages = [];

	}

	/**
	 *
	 * @param address
	 * set adress when adress in autocomplete change
	 */

	addressChanged(address: any) {
		this.addressObject = address;
	}

	/**
	 * Verify if user selected a good adress
	 */

	performNext() {
		if (this.addressObject === void 0) {
			this.messages.push({
				severity: 'error',
				summary: 'Validation error',
				detail: 'The address is invalid, please select an address from the drop-down menu'
			})
		} else {
			this.onNext.emit(this.addressObject);
		}
	}
}