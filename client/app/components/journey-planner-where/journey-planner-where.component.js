"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var JourneyPlannerWhereComponent = (function () {
    function JourneyPlannerWhereComponent() {
        this.onNext = new core_1.EventEmitter();
    }
    JourneyPlannerWhereComponent.prototype.ngOnInit = function () {
        this.messages = [];
    };
    /**
     *
     * @param address
     * set adress when adress in autocomplete change
     */
    JourneyPlannerWhereComponent.prototype.addressChanged = function (address) {
        this.addressObject = address;
    };
    /**
     * Verify if user selected a good adress
     */
    JourneyPlannerWhereComponent.prototype.performNext = function () {
        if (this.addressObject === void 0) {
            this.messages.push({
                severity: 'error',
                summary: 'Validation error',
                detail: 'The address is invalid, please select an address from the drop-down menu'
            });
        }
        else {
            this.onNext.emit(this.addressObject);
        }
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], JourneyPlannerWhereComponent.prototype, "onNext", void 0);
    JourneyPlannerWhereComponent = __decorate([
        core_1.Component({
            selector: 'journey-planner-where',
            template: require('./journey-planner-where.component.html'),
            styles: [require('./journey-planner-where.component.scss').toString()]
        }), 
        __metadata('design:paramtypes', [])
    ], JourneyPlannerWhereComponent);
    return JourneyPlannerWhereComponent;
}());
exports.JourneyPlannerWhereComponent = JourneyPlannerWhereComponent;
//# sourceMappingURL=journey-planner-where.component.js.map