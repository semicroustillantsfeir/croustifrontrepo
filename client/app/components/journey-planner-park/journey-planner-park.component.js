"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var journey_planner_service_1 = require("../../shared/services/journey-planner.service");
var constant_1 = require("../../shared/constant/constant");
var JourneyPlannerParkComponent = (function () {
    function JourneyPlannerParkComponent(_journeyPlannerService) {
        this._journeyPlannerService = _journeyPlannerService;
        this.onNext = new core_1.EventEmitter();
        this.AppConstant = constant_1.AppConstant;
        this.answer = [
            { label: 'YES', value: 'YES' },
            { label: 'NO', value: 'NO' }
        ];
        this.selectedAnswer = '';
    }
    JourneyPlannerParkComponent.prototype.ngOnInit = function () {
        this.getParks();
    };
    /**
     * Get Parks
     */
    JourneyPlannerParkComponent.prototype.getParks = function () {
        var _this = this;
        this._journeyPlannerService.getParks().subscribe(function (parks) {
            _this.parks = parks;
        });
    };
    /**
     *
     * @param event
     * go to next step if answer is yes
     */
    JourneyPlannerParkComponent.prototype.selectAnswer = function (event) {
        if (this.selectedAnswer.toUpperCase() === this.AppConstant.YesAnswer.toUpperCase()) {
            this.onNext.emit();
        }
    };
    /**
     *
     * @param park
     * select the park and go to the next step after select one
     */
    JourneyPlannerParkComponent.prototype.selectParking = function (park) {
        this.onNext.emit(park);
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], JourneyPlannerParkComponent.prototype, "onNext", void 0);
    JourneyPlannerParkComponent = __decorate([
        core_1.Component({
            selector: 'journey-planner-park',
            template: require('./journey-planner-park.component.html'),
            styles: [require('./journey-planner-park.component.scss').toString()]
        }), 
        __metadata('design:paramtypes', [journey_planner_service_1.JourneyPlannerService])
    ], JourneyPlannerParkComponent);
    return JourneyPlannerParkComponent;
}());
exports.JourneyPlannerParkComponent = JourneyPlannerParkComponent;
//# sourceMappingURL=journey-planner-park.component.js.map