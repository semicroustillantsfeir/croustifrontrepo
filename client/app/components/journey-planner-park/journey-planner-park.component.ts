import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import { JourneyPlannerService } from "../../shared/services/journey-planner.service";
import { Parks } from "../../model/park";
import {SelectItem} from "primeng/primeng";
import {AppConstant} from "../../shared/constant/constant";
import {ResultPark} from "../../model/result-park";

@Component({
    selector: 'journey-planner-park',
    template: require('./journey-planner-park.component.html'),
    styles:[require('./journey-planner-park.component.scss').toString()]
})
export class JourneyPlannerParkComponent implements OnInit {

    @Output() onNext = new EventEmitter<any>()
    private AppConstant = AppConstant;
    private parks:Parks[];
    private answer:SelectItem[] = [
        {label:'YES', value:'YES'},
        {label:'NO', value:'NO'}
    ];
    private selectedAnswer:string = '';

    constructor(private _journeyPlannerService:JourneyPlannerService) { }


    ngOnInit() {
        this.getParks();
    }


    /**
     * Get Parks
     */
    getParks(){
        this._journeyPlannerService.getParks().subscribe(parks=>{
            this.parks = parks
        })
    }

    /**
     *
     * @param event
     * go to next step if answer is yes
     */

    selectAnswer(event:any){
        if(this.selectedAnswer.toUpperCase() === this.AppConstant.YesAnswer.toUpperCase()){
            this.onNext.emit();
        }
    }

    /**
     *
     * @param park
     * select the park and go to the next step after select one
     */

    selectParking(park:ResultPark){
        this.onNext.emit(park)
    }

}