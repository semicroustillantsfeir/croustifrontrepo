import { Component, OnInit } from "@angular/core";
import { MenuItem } from "primeng/primeng";
import { GeolocationService } from "../../shared/services/geolocation.service";
import { JourneyPlannerService } from "../../shared/services/journey-planner.service";
import {AppService} from "../../shared/services/app.service";
import {Router, ActivatedRoute} from "@angular/router";
declare var google: any;

@Component({
	selector: 'journey-planner',
	template: require('./journey-planner.component.html'),
	styles: [require('./journey-planner.component.scss').toString()]
})
export class JourneyPlannerComponent implements OnInit {
	private steps: Array<MenuItem>;
	private activeStep: number;
	private journeyData: any;
	private currentPositionLoaded: boolean;
	private currentPositionError: boolean;
	private fallbackCurrentPosition: any;
	private park:any;

	constructor(private GeolocationService: GeolocationService,
				private JourneyPlannerService: JourneyPlannerService,
				private _appService:AppService,
				private _router:Router,
				private _route:ActivatedRoute) {
	}

	ngOnInit() {
		this.journeyData = {};
		this.activeStep = 0;
		this.currentPositionError = false;
		this.currentPositionLoaded = false;
		this.steps = [
			{label: 'Where ?'},
			{label: 'When ?'},
			{label: 'Can you park ?'},
			{label: 'Recapitulation'}
		];
		this.GeolocationService.getCurrentLocation(
			(position: any) => {
				this.journeyData.currentPosition = position;
				this.currentPositionLoaded = true;
			},
			(err: any) => {
				this.currentPositionLoaded = true;
				this.currentPositionError = true;
			});
		this._appService.showStep = true;
	}

	fallbackCurrentPositionSelected(location: any) {
		this.currentPositionError = false;
		this.fallbackCurrentPosition = location;
	}

	whereSelected(location: any) {
		this.activeStep = 1;
		this.journeyData.destination = location;
	}

	whenSelected(dateTimeData: {date: Date, type: string}) {
		this.journeyData.date = dateTimeData.date;
		this.journeyData.dateDeparture = dateTimeData.type === 'Departure';
		this.activeStep = 2;
	}

	parkSelected(park: any) {
		this.journeyData.park = park;
		if (!this.currentPositionError) {
			this.journeyData.departure = this.journeyData.currentPosition ? this.journeyData.currentPosition : this.fallbackCurrentPosition;
			this.activeStep = 3;
		}
	}

	confirm() {
		this._appService.trips = [];
		this.JourneyPlannerService.getTripsByTraffic(this.journeyData).subscribe(trips=>{
			this._appService.trips.push(trips);
			if(!this.journeyData.park){
				this.JourneyPlannerService.getTrips(this.journeyData).subscribe(trips=>{
					this._appService.trips.push(trips);
					this._router.navigate(['/trips'])
				});
			}else{
				this.JourneyPlannerService.getTripsByParking(this.journeyData).subscribe(trips=>{
					this._appService.trips.push(trips);
					this._router.navigate(['/trips'])
				});
			}
		});
	};


}