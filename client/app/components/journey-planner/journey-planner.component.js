"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var geolocation_service_1 = require("../../shared/services/geolocation.service");
var journey_planner_service_1 = require("../../shared/services/journey-planner.service");
var app_service_1 = require("../../shared/services/app.service");
var router_1 = require("@angular/router");
var JourneyPlannerComponent = (function () {
    function JourneyPlannerComponent(GeolocationService, JourneyPlannerService, _appService, _router, _route) {
        this.GeolocationService = GeolocationService;
        this.JourneyPlannerService = JourneyPlannerService;
        this._appService = _appService;
        this._router = _router;
        this._route = _route;
    }
    JourneyPlannerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.journeyData = {};
        this.activeStep = 0;
        this.currentPositionError = false;
        this.currentPositionLoaded = false;
        this.steps = [
            { label: 'Where ?' },
            { label: 'When ?' },
            { label: 'Can you park ?' },
            { label: 'Recapitulation' }
        ];
        this.GeolocationService.getCurrentLocation(function (position) {
            _this.journeyData.currentPosition = position;
            _this.currentPositionLoaded = true;
        }, function (err) {
            _this.currentPositionLoaded = true;
            _this.currentPositionError = true;
        });
        this._appService.showStep = true;
    };
    JourneyPlannerComponent.prototype.fallbackCurrentPositionSelected = function (location) {
        this.currentPositionError = false;
        this.fallbackCurrentPosition = location;
    };
    JourneyPlannerComponent.prototype.whereSelected = function (location) {
        this.activeStep = 1;
        this.journeyData.destination = location;
    };
    JourneyPlannerComponent.prototype.whenSelected = function (dateTimeData) {
        this.journeyData.date = dateTimeData.date;
        this.journeyData.dateDeparture = dateTimeData.type === 'Departure';
        this.activeStep = 2;
    };
    JourneyPlannerComponent.prototype.parkSelected = function (park) {
        this.journeyData.park = park;
        if (!this.currentPositionError) {
            this.journeyData.departure = this.journeyData.currentPosition ? this.journeyData.currentPosition : this.fallbackCurrentPosition;
            this.activeStep = 3;
        }
    };
    JourneyPlannerComponent.prototype.confirm = function () {
        var _this = this;
        this._appService.trips = [];
        this.JourneyPlannerService.getTripsByTraffic(this.journeyData).subscribe(function (trips) {
            _this._appService.trips.push(trips);
            if (!_this.journeyData.park) {
                _this.JourneyPlannerService.getTrips(_this.journeyData).subscribe(function (trips) {
                    _this._appService.trips.push(trips);
                    _this._router.navigate(['/trips']);
                });
            }
            else {
                _this.JourneyPlannerService.getTripsByParking(_this.journeyData).subscribe(function (trips) {
                    _this._appService.trips.push(trips);
                    _this._router.navigate(['/trips']);
                });
            }
        });
    };
    ;
    JourneyPlannerComponent = __decorate([
        core_1.Component({
            selector: 'journey-planner',
            template: require('./journey-planner.component.html'),
            styles: [require('./journey-planner.component.scss').toString()]
        }), 
        __metadata('design:paramtypes', [geolocation_service_1.GeolocationService, journey_planner_service_1.JourneyPlannerService, app_service_1.AppService, router_1.Router, router_1.ActivatedRoute])
    ], JourneyPlannerComponent);
    return JourneyPlannerComponent;
}());
exports.JourneyPlannerComponent = JourneyPlannerComponent;
//# sourceMappingURL=journey-planner.component.js.map