"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var app_service_1 = require("../../shared/services/app.service");
var angular_2_local_storage_1 = require('angular-2-local-storage');
var constant_1 = require("../../shared/constant/constant");
var TripComponent = (function () {
    function TripComponent(_appService, _localStorageService) {
        this._appService = _appService;
        this._localStorageService = _localStorageService;
        this.AppConstant = constant_1.AppConstant;
        this.titleAccordeon = (_a = {},
            _a["0"] = { "title": "Possibility without using your car" },
            _a["1"] = { "title": "Possibility with using your car" },
            _a
        );
        var _a;
    }
    TripComponent.prototype.ngOnInit = function () {
        this._appService.showStep = false;
        if (this._appService.trips) {
            this.trips = this._appService.trips;
            this._localStorageService.set(this.AppConstant.keyLocalStorage, this.trips);
        }
        else {
            this.trips = this._localStorageService.get(this.AppConstant.keyLocalStorage);
            console.log(this.trips);
        }
    };
    TripComponent = __decorate([
        core_1.Component({
            selector: 'trip',
            template: require('./trip.component.html'),
            styles: [require('./trip.component.scss').toString()]
        }), 
        __metadata('design:paramtypes', [app_service_1.AppService, angular_2_local_storage_1.LocalStorageService])
    ], TripComponent);
    return TripComponent;
}());
exports.TripComponent = TripComponent;
//# sourceMappingURL=trip.component.js.map