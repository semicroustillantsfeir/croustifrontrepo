import { Component, OnInit } from '@angular/core';
import {AppService} from "../../shared/services/app.service";
import { LocalStorageService } from 'angular-2-local-storage';
import { AppConstant } from "../../shared/constant/constant";

@Component({
    selector: 'trip',
    template:require('./trip.component.html'),
    styles:[require('./trip.component.scss').toString()]
})
export class TripComponent implements OnInit {
    private trips:any;
    private AppConstant = AppConstant;
    private titleAccordeon = {
        ["0"]:{"title":"Possibility without using your car"},
        ["1"]:{"title":"Possibility with using your car"}
    };

    constructor(private _appService:AppService, private _localStorageService:LocalStorageService) {
    }

    ngOnInit() {
        this._appService.showStep = false;
        if(this._appService.trips) {
          this.trips = this._appService.trips;
          this._localStorageService.set(this.AppConstant.keyLocalStorage, this.trips);
        }else {
            this.trips = this._localStorageService.get(this.AppConstant.keyLocalStorage);
            console.log(this.trips);
        }

    }

}