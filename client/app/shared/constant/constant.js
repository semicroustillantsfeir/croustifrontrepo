"use strict";
var AppConstant = (function () {
    function AppConstant() {
    }
    Object.defineProperty(AppConstant, "addressIp", {
        get: function () { return 'http://10.0.109.142:8080/api'; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(AppConstant, "YesAnswer", {
        get: function () { return 'YES'; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(AppConstant, "NoAnswer", {
        get: function () { return 'NO'; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(AppConstant, "keyLocalStorage", {
        get: function () { return 'PARK'; },
        enumerable: true,
        configurable: true
    });
    ;
    return AppConstant;
}());
exports.AppConstant = AppConstant;
//# sourceMappingURL=constant.js.map