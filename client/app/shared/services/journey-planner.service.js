"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require("@angular/http");
var constant_1 = require("../constant/constant");
var JourneyPlannerService = (function () {
    function JourneyPlannerService(_http) {
        this._http = _http;
        this.Appconstant = constant_1.AppConstant;
    }
    JourneyPlannerService.prototype.getParks = function () {
        return this._http.get(this.Appconstant.addressIp + '/parks').map(function (parks) {
            return parks.json();
        });
    };
    JourneyPlannerService.prototype.getTrips = function (journeyData) {
        var apiBodyDefault = this.journeyDateToTripBody(journeyData);
        return this._http.post(this.Appconstant.addressIp + '/trip', Object.assign({}, apiBodyDefault, { mode: 'DRIVING' })).map(function (trips) {
            return trips.json();
        });
    };
    JourneyPlannerService.prototype.getTripsByParking = function (journeyData) {
        var apiBodyDefault = this.journeyDateToTripBody(journeyData);
        return this._http.post(this.Appconstant.addressIp + '/trip/' + journeyData.park.id, Object.assign({}, apiBodyDefault, { mode: 'DRIVING' })).map(function (trips) {
            return trips.json();
        });
    };
    JourneyPlannerService.prototype.getTripsByTraffic = function (journeyData) {
        var apiBodyDefault = this.journeyDateToTripBody(journeyData);
        return this._http.post(this.Appconstant.addressIp + '/trip', Object.assign({}, apiBodyDefault, { mode: 'TRANSIT' })).map(function (trips) {
            return trips.json();
        });
    };
    JourneyPlannerService.prototype.journeyDateToTripBody = function (journeyData) {
        return {
            startLocation: journeyData.departure.coords ? journeyData.departure.coords.latitude + ', ' + journeyData.departure.coords.longitude : journeyData.departure.geometry.location.lat() + ', ' + journeyData.departure.geometry.location.lng(),
            finishLocation: journeyData.destination.geometry.location.lat() + ', ' + journeyData.destination.geometry.location.lng(),
            alternatives: true,
            departureTime: journeyData.dateDeparture ? journeyData.date.getTime() / 1000 : null,
        };
    };
    JourneyPlannerService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], JourneyPlannerService);
    return JourneyPlannerService;
}());
exports.JourneyPlannerService = JourneyPlannerService;
//# sourceMappingURL=journey-planner.service.js.map