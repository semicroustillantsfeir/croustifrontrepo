import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import {Http} from "@angular/http";
import {Parks} from "../../model/park";
import {AppConstant} from "../constant/constant";

@Injectable()
export class JourneyPlannerService {
    Appconstant = AppConstant;

    constructor(private _http:Http) {}

    getParks():Observable <Parks[]> {
        return this._http.get(this.Appconstant.addressIp+'/parks').map(parks=>{
            return <Parks[]>parks.json()
        })
    }

    getTrips(journeyData: any): Observable <any[]> {
        const apiBodyDefault = this.journeyDateToTripBody(journeyData);
        return this._http.post(this.Appconstant.addressIp+'/trip', Object.assign({}, apiBodyDefault,{mode:'DRIVING'})).map(trips => {
            return <any[]>trips.json();
        });
    }

    getTripsByParking(journeyData:any):Observable<any[]> {
        const apiBodyDefault = this.journeyDateToTripBody(journeyData);
        return this._http.post(this.Appconstant.addressIp+'/trip/'+journeyData.park.id,Object.assign({}, apiBodyDefault,{mode:'DRIVING'})).map(trips=>{
            return <any[]>trips.json()
        })
    }

    getTripsByTraffic(journeyData:any):Observable<any[]> {
        const apiBodyDefault = this.journeyDateToTripBody(journeyData);
        return this._http.post(this.Appconstant.addressIp+'/trip',Object.assign({}, apiBodyDefault,{mode:'TRANSIT'})).map(trips=>{
            return <any[]>trips.json()
        })

    }

    journeyDateToTripBody(journeyData: any) {
        return {
            startLocation: journeyData.departure.coords ? journeyData.departure.coords.latitude + ', ' + journeyData.departure.coords.longitude : journeyData.departure.geometry.location.lat() + ', ' + journeyData.departure.geometry.location.lng(),
            finishLocation: journeyData.destination.geometry.location.lat() + ', ' + journeyData.destination.geometry.location.lng(),
            alternatives: true,
            departureTime: journeyData.dateDeparture ? journeyData.date.getTime()/1000 : null,
        }
    }
}