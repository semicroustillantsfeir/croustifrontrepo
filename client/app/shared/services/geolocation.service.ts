import { Injectable } from "@angular/core";

@Injectable()
export class GeolocationService {

	constructor() {

	}

	getCurrentLocation(successCallback: Function, errorCallback: Function, options?: any): any {
		return window.navigator.geolocation.getCurrentPosition(
			(position) => {
				const crd = position.coords;
				if (successCallback) {
					return successCallback(position);
				}
			}, (err) => {
				console.warn(`ERROR(${err.code}): ${err.message}`);
				if (errorCallback) {
					return errorCallback(err);
				}
			},
			Object.assign({}, {
				enableHighAccuracy: true,
				timeout: 5000,
				maximumAge: 0
			}, options));
	}

}