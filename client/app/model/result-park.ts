export interface ResultPark {
    id:string;
    name:string;
    availableSpaces:number;
    totalSpaces:number;
    trend:string;
    open:boolean;
    address:string;

}