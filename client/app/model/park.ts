import { ResultPark } from "./result-park";
export interface Parks{
    result:ResultPark[];
    error:boolean;
    errorMessage:string;
    codeStatus:number
}