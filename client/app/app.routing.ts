import {Routes, RouterModule} from '@angular/router';
import {TripComponent} from "./components/trip/trip.component";

const appRoutes: Routes = [
    {
        path:'trips',
        component: TripComponent
    }
];

export const appRoutingProviders: any[] = [];

export const routing = RouterModule.forRoot(appRoutes);
